# Manyver.se website

If you want to contribute changes to the website, first of all, thank you!

- Note our [Code of conduct](https://gitlab.com/staltz/manyverse/blob/master/code-of-conduct.md)
- If you want to **improve** the site, first discuss it as an issue before opening any merge request
- If you want to **fix** a small issue, any merge request is accepted, with prior issue or not

The website is coded in HTML by hand, not generated from any tool. Take that in mind when contributing changes. It's important to keep the static HTML simple and small so that it loads quickly on various browsers.
